import sys


def generate_binary(string, position=0):
    if position == len(string):
        print(string)
    else:
        if string[position] == 'X':
            generate_binary(string[:position] + '0' + string[position + 1:],
                            position + 1)
            generate_binary(string[:position] + '1' + string[position + 1:],
                            position + 1)
        else:
            generate_binary(string, position + 1)


def main():
    s = sys.argv[1]
    generate_binary(s)


main()
