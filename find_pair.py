import sys
from bisect import bisect_left
from collections import OrderedDict


def find_pair(gifts, goal):
    best_price = 0
    best_items = ()
    for price, gift in gifts.items():
        remainder = goal - price
        # if the remainder is in gifts and its not the current we have an exact match
        if remainder in gifts and remainder != price:
            return (gifts[remainder], remainder), (gift, price)

        # otherwise we need to find the best gift to go with the current
        prices = list(gifts.keys())
        del prices[bisect_left(prices, price)]  # remove the current so we don't pick it twice
        closest_index = bisect_left(prices, remainder) - 1
        closest = prices[closest_index]
        current = price + closest
        # if the current two are closer than the previous best take note
        if current > best_price and current < goal:
            best_price = current
            best_items = ((gifts[closest], closest), (gift, price))
    return best_items


def main():
    fname = sys.argv[1]
    goal = int(sys.argv[2])
    gifts = OrderedDict()
    with open(fname) as f:
        for line in f:
            gift, price = list(line.split(','))
            gifts[int(price)] = gift
    results = find_pair(gifts, goal)
    if results:
        print(results[0], results[1], 'Total:', results[0][1] + results[1][1])
    else:
        print('Not possible')


if __name__ == '__main__':
    main()
