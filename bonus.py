import sys


def memoize(f):
    # caches the results of function f
    memo = {}

    def helper(i, j):
        if (i, j) not in memo:
            memo[(i, j)] = f(i, j)
        return memo[(i, j)]

    return helper


def bonus(items, goal):
    # return the value of the most valuable set of the first i
    # whose value sum to less than or equal to j.
    @memoize
    def best_sublist(i, j):
        if i == 0:
            return 0
        value = items[i - 1][1]
        # if the current is greater than our budget skip it
        if value > j:
            return best_sublist(i - 1, j)
        # otherwise return the best of skipping the current and including it
        else:
            return max(best_sublist(i - 1, j), 
                       best_sublist(i - 1, j - value) + value)

    j = goal
    results = []
    for i in range(len(items), 0, -1):
        # if the two sublists are not equal than we take the next element
        # otherwise the last element must not be in the solution so we skip it
        if best_sublist(i, j) != best_sublist(i - 1, j):
            results.append(items[i - 1])
            j -= items[i - 1][1]
    results.reverse()
    return results, sum([x for (_, x) in results])


def main():
    fname = sys.argv[1]
    goal = int(sys.argv[2])
    items = []
    with open(fname) as f:
        for line in f:
            item, price = list(line.split(','))
            items.append((item, int(price)))
    results = bonus(items, goal)
    print(' '.join([str(item) for item in results[0]]), 'Total:', results[1])


if __name__ == '__main__':
    main()
