#!flask/bin/python

from hashlib import sha256
from flask_mysqldb import MySQL
from flask import Flask, jsonify, abort, request, make_response

mysql = MySQL()
app = Flask(__name__)
app.config['MYSQL_USER'] = 'message_app'
app.config['MYSQL_DB'] = 'paxos'
mysql.init_app(app)


@app.route('/messages/<string:hash_id>', methods=['GET'])
def get_message(hash_id):
    message = get_row(hash_id)
    if message:
        return jsonify({'message': message[0][1]})
    abort(404)


@app.route('/messages', methods=['POST'])
def create_hash():
    if not request.json or not 'message' in request.json:
        abort(400)

    sha_message = sha256(request.json['message'].encode('utf-8'))
    digest = sha_message.hexdigest()
    message = get_row(digest)

    # if the message has already been added just return it
    if message:
        return jsonify({'disgest': message[0][2]}), 201
    # otherwise add it add return it
    conn = mysql.connection
    cursor = conn.cursor()
    cursor.execute("INSERT INTO messages (message, hash) VALUES (%s,%s)", (request.json['message'], digest))
    conn.commit()
    return jsonify({'digest': digest}), 201


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'message not found'}), 404)


@app.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'no message included'}), 400)


def get_row(hash_id):
    # gets row by hash 
    conn = mysql.connection
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM messages WHERE hash = %s", [hash_id])
    return cursor.fetchall()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000')
